package ru.karamyshev.taskmanager.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.api.endpoint.*;
import ru.karamyshev.taskmanager.api.service.IPropertyService;
import ru.karamyshev.taskmanager.api.service.IUserService;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.xml.ws.Endpoint;

@Component
@Scope("singleton")
public class Bootstrap {

    private IPropertyService propertyService;

    private IUserService userService;

    private ITaskEndpoint taskEndpoint;

    private IUserEndpoint userEndpoint;

    private IAdminEndpoint adminEndpoint;

    private IAdminUserEndpoint adminUserEndpoint;

    private IProjectEndpoint projectEndpoint;

    private ISessionEndpoint sessionEndpoint;

    private IAuthenticationEndpoint authenticationEndpoint;

    public Bootstrap(
            @NotNull final IPropertyService propertyService,
            @NotNull final IUserService userService,
            @NotNull final ITaskEndpoint taskEndpoint,
            @NotNull final IUserEndpoint userEndpoint,
            @NotNull final IAdminEndpoint adminEndpoint,
            @NotNull final IAdminUserEndpoint adminUserEndpoint,
            @NotNull final IProjectEndpoint projectEndpoint,
            @NotNull final ISessionEndpoint sessionEndpoint,
            @NotNull final IAuthenticationEndpoint authenticationEndpoint
    ) {
        this.propertyService = propertyService;
        this.userService = userService;
        this.taskEndpoint = taskEndpoint;
        this.userEndpoint = userEndpoint;
        this.adminEndpoint = adminEndpoint;
        this.adminUserEndpoint = adminUserEndpoint;
        this.projectEndpoint = projectEndpoint;
        this.sessionEndpoint = sessionEndpoint;
        this.authenticationEndpoint = authenticationEndpoint;
    }

    private void initEndpoint() {
        @NotNull final String url = "http://" + propertyService.getServerHost() + ":" + propertyService.getServerPort();
        @NotNull final String sessionEndpointUrl = url + "/SessionEndpoint?wsdl";
        @NotNull final String taskEndpointUrl = url + "/TaskEndpoint?wsdl";
        @NotNull final String projectEndpointUrl = url + "/ProjectEndpoint?wsdl";
        @NotNull final String userEndpointUrl = url + "/UserEndpoint?wsdl";
        @NotNull final String adminUserEndpointUrl = url + "/AdminUserEndpoint?wsdl";
        @NotNull final String adminEndpointUrl = url + "/AdminEndpoint?wsdl";
        @NotNull final String authenticationEndpointUrl = url + "/AuthenticationEndpoint?wsdl";

        Endpoint.publish(authenticationEndpointUrl, authenticationEndpoint);
        Endpoint.publish(sessionEndpointUrl, sessionEndpoint);
        Endpoint.publish(taskEndpointUrl, taskEndpoint);
        Endpoint.publish(projectEndpointUrl, projectEndpoint);
        Endpoint.publish(userEndpointUrl, userEndpoint);
        Endpoint.publish(adminEndpointUrl, adminEndpoint);
        Endpoint.publish(adminUserEndpointUrl, adminUserEndpoint);
    }

    private void initProperty() {
        propertyService.init();
    }

    private void initUser() {
        userService.create("admin", "admin", Role.ADMIN);
        userService.create("test", "test", "test@test.com");
    }

    public void run(final String[] args) {
        initProperty();
        initUser();
        initEndpoint();
        System.out.println("*** TASK MANAGER SERVER ***");
    }

}
