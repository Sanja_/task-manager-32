package ru.karamyshev.taskmanager.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.api.repository.IRepository;
import ru.karamyshev.taskmanager.api.service.IService;
import ru.karamyshev.taskmanager.entity.AbstractEntitty;

import java.util.List;

@Component
public abstract class AbstractService<E extends AbstractEntitty> implements IService<E> {

    @Autowired
    protected AnnotationConfigApplicationContext context;

    @Nullable
    private IRepository<E> iRepository;

    @Nullable
    @Override
    public List<E> getList() {
        return iRepository.getList();
    }

    @Override
    public void load(@Nullable List<E> eList) {
        if (eList == null) return;
        iRepository.load(eList);
    }

}
