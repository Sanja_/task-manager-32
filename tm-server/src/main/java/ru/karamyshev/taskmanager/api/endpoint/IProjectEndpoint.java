package ru.karamyshev.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.Project;
import ru.karamyshev.taskmanager.entity.Session;

import java.util.List;

public interface IProjectEndpoint {

    void createNameProject(
            @Nullable Session session,
            @Nullable String name
    ) throws Exception;

    void createDescriptionProject(
            @Nullable Session session,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    void addProject(
            @Nullable Session session,
            @Nullable Project project
    ) throws Exception;

    void removeProject(
            @Nullable Session session,
            @Nullable Project project
    ) throws Exception;


    @Nullable
    List<Project> findAllProject(
            @Nullable Session session
    ) throws Exception;

    void clearProject(
            @Nullable Session session
    ) throws Exception;

    @Nullable
    Project findOneProjectByIndex(
            @Nullable Session session,
            @Nullable Integer index
    ) throws Exception;

    @Nullable
    List<Project> findOneProjectByName(
            @Nullable Session session,
            @Nullable String name
    ) throws Exception;

    @Nullable
    Project updateProjectById(
            @Nullable Session session,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    @Nullable
    Project removeOneProjectByIndex(
            @Nullable Session session,
            @Nullable Integer index
    ) throws Exception;

    @Nullable
    List<Project> removeOneProjectByName(
            @Nullable Session session,
            @Nullable String name
    ) throws Exception;

    @Nullable
    Project findOneProjectById(
            @Nullable Session session,
            @Nullable String id
    ) throws Exception;

    @Nullable
    Project removeOneProjectById(
            @Nullable Session session,
            @Nullable String id
    ) throws Exception;

    @Nullable
    Project updateProjectByIndex(
            @Nullable Session session,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    @Nullable
    List<Project> getProjectList(
            @Nullable Session session
    ) throws Exception;

    void loadProject(
            @Nullable Session session,
            @Nullable List<Project> projects
    ) throws Exception;

}
