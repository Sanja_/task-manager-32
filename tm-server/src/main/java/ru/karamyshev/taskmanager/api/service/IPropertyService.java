package ru.karamyshev.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

public interface IPropertyService {

    void init();

    @NotNull
    String getServerHost();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getSessionSalt();

    @NotNull
    Integer getSessionCycle();

}
