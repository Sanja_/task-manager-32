package ru.karamyshev.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;

import java.util.List;

public interface ISessionService extends IService<Session> {

    void close(@Nullable Session session) throws Exception;

    void closeAll(@Nullable Session session) throws Exception;

    @Nullable
    User getUser(@Nullable Session session) throws Exception;

    @Nullable
    String getUserId(@Nullable Session session) throws Exception;

    @Nullable
    List<Session> getListSession(Session session) throws Exception;

    @Nullable
    Session sign(@Nullable Session session);

    boolean isValid(@Nullable Session session);

    void validate(@Nullable Session session) throws Exception;

    void validate(@Nullable Session session, @Nullable Role role) throws Exception;

    @Nullable
    Session open(@Nullable String login, @Nullable String password);

    boolean checkDataAccess(@Nullable String login, @Nullable String password);

    void signOutByLogin(@Nullable String login) throws Exception;

    void signOutByUserId(@Nullable String userId) throws Exception;
}
