package ru.karamyshev.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    void add(@NotNull String userId, @NotNull Project project);

    void remove(@NotNull String userId, @NotNull Project project);

    @Nullable
    List<Project> findAll(@NotNull String userId);

    void clear(@NotNull String userId);

    @NotNull
    Project findOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project removeOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    List<Project> findOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Project removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    List<Project> removeOneByName(@NotNull String userId, @NotNull String name);

}
