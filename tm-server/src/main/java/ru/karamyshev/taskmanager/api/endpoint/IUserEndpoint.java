package ru.karamyshev.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IUserEndpoint {

    @Nullable
    @WebMethod
    User createUser(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password
    ) throws Exception;

    @Nullable
    @WebMethod
    User createUserEmail(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "email", partName = "email") @Nullable String email
    ) throws Exception;

    @Nullable
    @WebMethod
    User createUserRole(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "role", partName = "role") @Nullable Role role
    ) throws Exception;

    @Nullable
    @WebMethod
    User findUserById(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    User findUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception;

    @Nullable
    @WebMethod
    User removeUser(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "user", partName = "user") @Nullable User user
    ) throws Exception;

    @Nullable
    @WebMethod
    User removeUserById(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    User removeByLogin(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception;

    @Nullable
    @WebMethod
    User lockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "currentLogin", partName = "currentLogin") @Nullable String currentLogin,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception;

    @Nullable
    @WebMethod
    User unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception;

    @Nullable
    @WebMethod
    User removeUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception;

    @Nullable
    @WebMethod
    List<User> getUserList(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception;

    @WebMethod
    void loadUser(
            @WebParam(name = "users", partName = "users") @Nullable List<User> users,
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception;

}
