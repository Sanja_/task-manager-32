package ru.karamyshev.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.karamyshev.taskmanager.api.repository.ITaskRepository;
import ru.karamyshev.taskmanager.entity.Task;

import java.util.ArrayList;
import java.util.List;

@Repository
public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void add(@NotNull final String userId, @NotNull final Task task) {
        task.setUserId(userId);
        entities.add(task);
    }

    @Override
    public void remove(
            final @NotNull String userId,
            final @NotNull Task task) {
        entities.remove(task);
    }

    @Nullable
    @Override
    public List<Task> findAll(@NotNull final String userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : entities) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void clear(final @NotNull String userId) {
        final List<Task> task = findAll(userId);
        entities.removeAll(task);
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String userId, @NotNull final String id) {
        for (final Task task : entities) {
            if (userId.equals(task.getUserId())) {
                if (Long.parseLong(id) == task.getId()) return task;
            }
        }
        return null;
    }

    @Nullable
    @Override
    public Task removeOneById(@NotNull final String userId, @NotNull final String id) {
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        entities.remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        for (final Task task : entities) {
            if (userId.equals(task.getUserId())) {
                if (entities.indexOf(task) == index) return task;

            }
        }
        return null;
    }

    @Nullable
    @Override
    public List<Task> findOneByName(@NotNull final String userId, @NotNull final String name) {
        final List<Task> projectsSample = new ArrayList<>();
        for (final Task task : entities) {
            if (userId.equals(task.getUserId())) {
                if (name.equals(task.getName())) projectsSample.add(task);
            }
        }
        return projectsSample;
    }

    @Nullable
    @Override
    public Task removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Nullable
    @Override
    public List<Task> removeOneByName(@NotNull final String userId, @NotNull final String name) {
        final List<Task> tasks = findOneByName(userId, name);
        if (tasks == null) return null;
        for (Task task : tasks) {
            remove(userId, task);
        }
        return tasks;
    }

}
