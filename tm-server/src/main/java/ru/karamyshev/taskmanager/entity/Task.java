package ru.karamyshev.taskmanager.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class Task extends AbstractEntitty implements Serializable {

    private static final long serialVersionUID = 1001L;

    @NotNull
    private String name = "default name task";

    @Nullable
    private String description = "default description task";

    @NotNull
    private String userId;

    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}
