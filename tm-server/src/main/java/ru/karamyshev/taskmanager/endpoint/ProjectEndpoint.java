package ru.karamyshev.taskmanager.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.karamyshev.taskmanager.api.endpoint.IProjectEndpoint;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.api.service.ISessionService;
import ru.karamyshev.taskmanager.entity.Project;
import ru.karamyshev.taskmanager.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@Controller
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @Nullable
    @Autowired
    private ISessionService sessionService;

    @Nullable
    @Autowired
    private IProjectService projectService;

    @Override
    @WebMethod
    public void createNameProject(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name
    ) throws Exception {
        sessionService.validate(session);
        projectService.create(session.getUserId(), name);
    }


    @Override
    @WebMethod
    public void addProject(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "project", partName = "project") @Nullable Project project
    ) throws Exception {
        sessionService.validate(session);
        projectService.add(session.getUserId(), project);
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "project", partName = "project") @Nullable Project project
    ) throws Exception {
        sessionService.validate(session);
        projectService.remove(session.getUserId(), project);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Project> findAllProject(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception {
        sessionService.validate(session);
        return projectService.findAll(session.getUserId());
    }

    @Override
    @WebMethod
    public void clearProject(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception {
        sessionService.validate(session);
        projectService.clear(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public Project findOneProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "projectIndex", partName = "projectIndex") @Nullable Integer index
    ) throws Exception {
        sessionService.validate(session);
        return projectService.findOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Project> findOneProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "projectName", partName = "projectName") @Nullable String name
    ) throws Exception {
        sessionService.validate(session);
        return projectService.findOneByName(session.getUserId(), name);
    }

    @Nullable
    @Override
    @WebMethod
    public Project updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String id,
            @WebParam(name = "projectUpName", partName = "projectUpName") @Nullable String name,
            @WebParam(name = "projectUpDescription", partName = "projectUpDescription") @Nullable String description
    ) throws Exception {
        sessionService.validate(session);
        return projectService.updateProjectById(session.getUserId(), id, name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public Project removeOneProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "projectIndex", partName = "projectIndex") @Nullable Integer index
    ) throws Exception {
        sessionService.validate(session);
        return projectService.removeOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Project> removeOneProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "projectName", partName = "projectName") @Nullable String name
    ) throws Exception {
        sessionService.validate(session);
        return projectService.removeOneByName(session.getUserId(), name);
    }

    @Nullable
    @Override
    @WebMethod
    public Project findOneProjectById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String id
    ) throws Exception {
        sessionService.validate(session);
        return projectService.findOneById(session.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public Project removeOneProjectById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String id
    ) throws Exception {
        sessionService.validate(session);
        return projectService.removeOneById(session.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public Project updateProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "projectIndex", partName = "projectIndex") @Nullable Integer index,
            @WebParam(name = "projectName", partName = "projectName") @Nullable String name,
            @WebParam(name = "projectDescription", partName = "projectDescription") @Nullable String description
    ) throws Exception {
        sessionService.validate(session);
        return projectService.updateProjectByIndex(session.getUserId(), index, name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Project> getProjectList(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception {
        sessionService.validate(session);
        return projectService.getList();
    }

    @Override
    @WebMethod
    public void loadProject(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "projects", partName = "projects") @Nullable List<Project> projects
    ) throws Exception {
        sessionService.validate(session);
        projectService.load(projects);
    }

    @Override
    @WebMethod
    public void createDescriptionProject(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @Nullable String description
    ) throws Exception {
        sessionService.validate(session);
        projectService.create(session.getUserId(), name, description);
    }

}
