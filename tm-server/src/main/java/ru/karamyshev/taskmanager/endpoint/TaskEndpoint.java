package ru.karamyshev.taskmanager.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.karamyshev.taskmanager.api.endpoint.ITaskEndpoint;
import ru.karamyshev.taskmanager.api.service.ISessionService;
import ru.karamyshev.taskmanager.api.service.ITaskService;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@Controller
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @Nullable
    @Autowired
    private ISessionService sessionService;

    @Nullable
    @Autowired
    private ITaskService taskService;

    @Override
    @WebMethod
    public void createName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "nameTask", partName = "nameTask") @Nullable String name
    ) throws Exception {
        sessionService.validate(session);
        taskService.create(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void createDescription(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "nameTask", partName = "nameTask") @Nullable String name,
            @WebParam(name = "descriptionTask", partName = "descriptionTask") @Nullable String description
    ) throws Exception {
        sessionService.validate(session);
        taskService.create(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public void addTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "task", partName = "task") @Nullable Task task
    ) throws Exception {
        sessionService.validate(session);
        taskService.add(session.getUserId(), task);
    }

    @Override
    @WebMethod
    public void removeTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "task", partName = "task") @Nullable Task task
    ) throws Exception {
        sessionService.validate(session);
        taskService.remove(session.getUserId(), task);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Task> findAllTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception {
        sessionService.validate(session);
        return taskService.findAll(session.getUserId());
    }

    @Override
    @WebMethod
    public void clearTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception {
        sessionService.validate(session);
        taskService.clear(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public Task findTaskOneById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String id
    ) throws Exception {
        sessionService.validate(session);
        return taskService.findOneById(session.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public Task findTaskOneByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "taskIndex", partName = "taskIndex") @Nullable Integer index
    ) throws Exception {
        sessionService.validate(session);
        return taskService.findOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Task> findTaskOneByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "taskName", partName = "taskName") @Nullable String name
    ) throws Exception {
        sessionService.validate(session);
        return taskService.findOneByName(session.getUserId(), name);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTaskOneById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String id
    ) throws Exception {
        sessionService.validate(session);
        return taskService.removeOneById(session.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTaskOneByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "taskIndex", partName = "taskIndex") @Nullable Integer index
    ) throws Exception {
        sessionService.validate(session);
        return taskService.removeOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Task> removeTaskOneByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "taskName", partName = "taskName") @Nullable String name
    ) throws Exception {
        sessionService.validate(session);
        return taskService.removeOneByName(session.getUserId(), name);
    }

    @Nullable
    @Override
    @WebMethod
    public Task updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String id,
            @WebParam(name = "taskName", partName = "taskName") @Nullable String name,
            @WebParam(name = "taskDescription", partName = "taskDescription") @Nullable String description
    ) throws Exception {
        sessionService.validate(session);
        return taskService.updateTaskById(session.getUserId(), id, name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public Task updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "taskIndex", partName = "taskIndex") @Nullable Integer index,
            @WebParam(name = "taskName", partName = "taskName") @Nullable String name,
            @WebParam(name = "taskDescription", partName = "taskDescription") @Nullable String description
    ) throws Exception {
        sessionService.validate(session);
        return taskService.updateTaskByIndex(session.getUserId(), index, name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Task> getTaskList(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception {
        sessionService.validate(session);
        return taskService.getList();
    }

    @Override
    @WebMethod
    public void loadTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "tasks", partName = "tasks") @Nullable List<Task> tasks
    ) throws Exception {
        sessionService.validate(session);
        taskService.load(tasks);
    }

}
