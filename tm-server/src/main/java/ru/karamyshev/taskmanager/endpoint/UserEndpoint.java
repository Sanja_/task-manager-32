package ru.karamyshev.taskmanager.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.karamyshev.taskmanager.api.endpoint.IUserEndpoint;
import ru.karamyshev.taskmanager.api.service.ISessionService;
import ru.karamyshev.taskmanager.api.service.IUserService;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@Controller
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @Nullable
    @Autowired
    private IUserService userService;

    @Nullable
    @Autowired
    private ISessionService sessionService;

    @Nullable
    @Override
    @WebMethod
    public User createUser(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password
    ) throws Exception {
        return userService.create(login, password);
    }

    @Nullable
    @Override
    @WebMethod
    public User createUserEmail(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "email", partName = "email") @Nullable String email
    ) throws Exception {
        return userService.create(login, password, email);
    }

    @Nullable
    @Override
    @WebMethod
    public User createUserRole(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "role", partName = "role") @Nullable Role role
    ) throws Exception {
        return userService.create(login, password, role);
    }

    @Nullable
    @Override
    @WebMethod
    public User findUserById(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception {
        sessionService.validate(session);
        return userService.findById(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public User findUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception {
        sessionService.validate(session);
        return userService.findByLogin(login);
    }

    @Nullable
    @Override
    @WebMethod
    public User removeUser(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "user", partName = "user") @Nullable User user
    ) throws Exception {
        sessionService.validate(session);
        return userService.removeUser(user);
    }

    @Nullable
    @Override
    @WebMethod
    public User removeUserById(
            @WebParam(name = "session", partName = "session") @Nullable Session session/*,
            @WebParam (name = "id", partName = "id") @Nullable String id*/
    ) throws Exception {
        sessionService.validate(session);
        return userService.removeById(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public User removeByLogin(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception {
        sessionService.validate(session);
        return userService.removeByLogin(login);
    }

    @Nullable
    @Override
    @WebMethod
    public User lockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "currentLogin", partName = "currentLogin") @Nullable String currentLogin,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception {
        sessionService.validate(session);
        return userService.lockUserByLogin(currentLogin, login);
    }

    @Nullable
    @Override
    @WebMethod
    public User unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception {
        sessionService.validate(session);
        return userService.unlockUserByLogin(login);
    }

    @Nullable
    @Override
    @WebMethod
    public User removeUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception {
        sessionService.validate(session);
        final User currentUser = userService.findById(session.getUserId());
        return userService.removeUserByLogin(currentUser.getLogin(), login);
    }

    @Nullable
    @Override
    @WebMethod
    public List<User> getUserList(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception {
        sessionService.validate(session);
        return userService.getList();
    }

    @Override
    @WebMethod
    public void loadUser(
            @WebParam(name = "users", partName = "users") @Nullable List<User> users,
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception {
        sessionService.validate(session);
        userService.load(users);
    }

}
