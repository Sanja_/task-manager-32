package ru.karamyshev.taskmanager.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.karamyshev.taskmanager.api.endpoint.IAdminUserEndpoint;
import ru.karamyshev.taskmanager.api.service.IAuthenticationService;
import ru.karamyshev.taskmanager.api.service.ISessionService;
import ru.karamyshev.taskmanager.api.service.IUserService;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    @Nullable
    @Autowired
    private ISessionService sessionService;

    @Nullable
    @Autowired
    private IUserService userService;

    @Nullable
    @Autowired
    private IAuthenticationService authenticationService;

    @Nullable
    @Override
    @WebMethod
    public User createUserWithEmail(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "email", partName = "email") @Nullable final String email
    ) throws Exception {
        return userService.create(login, password, email);

    }

    @Nullable
    @Override
    @WebMethod
    public User createUserWithRole(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "password", partName = "password") final String password,
            @WebParam(name = "role", partName = "role") final Role role
    ) throws Exception {
        sessionService.validate(session, Role.ADMIN);
        return userService.create(login, password, role);
    }

    @Nullable
    @Override
    @WebMethod
    public User lockUser(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "login", partName = "login") final String login
    ) throws Exception {
        sessionService.validate(session);
        final String currentLogin = authenticationService.getCurrentLogin();
        return userService.lockUserByLogin(login, currentLogin);
    }

    @Nullable
    @Override
    @WebMethod
    public User unlockUser(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "login", partName = "login") final String login
    ) throws Exception {
        sessionService.validate(session);
        return userService.unlockUserByLogin(login);
    }

    @Nullable
    @Override
    @WebMethod
    public User removeUsByLog(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "login", partName = "login") final String login
    ) throws Exception {
        sessionService.validate(session);
        return userService.removeByLogin(login);
    }

}
