package ru.karamyshev.taskmanager.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.Task;
import ru.karamyshev.taskmanager.endpoint.TaskEndpoint;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.listener.AbstractListener;
import ru.karamyshev.taskmanager.service.SessionService;
import ru.karamyshev.taskmanager.util.TerminalUtil;

@Component
public class TaskRemoveByIndexListener extends AbstractListener {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String arg() {
        return "-tskrmvind";
    }

    @NotNull
    @Override
    public String command() {
        return "task-remove-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by index.";
    }

    @Override
    @EventListener(condition = "@taskRemoveByIndexListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        @Nullable final Session session = sessionService.getSession();
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER TASK INDEX FOR DELETION:");
        @Nullable final Integer index = TerminalUtil.nextNumber();
        @Nullable final Task task = taskEndpoint.removeTaskOneByIndex(session, index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
