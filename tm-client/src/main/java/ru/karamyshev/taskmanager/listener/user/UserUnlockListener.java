package ru.karamyshev.taskmanager.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.endpoint.Exception_Exception;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.UserEndpoint;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.listener.AbstractListener;
import ru.karamyshev.taskmanager.service.SessionService;
import ru.karamyshev.taskmanager.util.TerminalUtil;

@Component
public class UserUnlockListener extends AbstractListener {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @NotNull
    @Override
    public String arg() {
        return "-unlckusr";
    }

    @NotNull
    @Override
    public String command() {
        return "unlock-user";
    }

    @NotNull
    @Override
    public String description() {
        return "unlocked users";
    }

    @Override
    @EventListener(condition = "@userUnlockListener.command() == #event.name")
    public void handler(ConsoleEvent event) throws Exception_Exception {
        System.out.println("[UNLOCK USER]");
        System.out.println("[ENTER LOGIN]");
        @Nullable final String login = TerminalUtil.nextLine();
        @Nullable final Session session = sessionService.getSession();
        userEndpoint.unlockUserByLogin(session, login);

        System.out.println("[OK]");
    }

}
