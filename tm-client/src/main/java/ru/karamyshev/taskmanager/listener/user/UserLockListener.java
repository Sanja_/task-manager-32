package ru.karamyshev.taskmanager.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.endpoint.AdminUserEndpoint;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.listener.AbstractListener;
import ru.karamyshev.taskmanager.service.SessionService;
import ru.karamyshev.taskmanager.util.TerminalUtil;

@Component
public class UserLockListener extends AbstractListener {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @NotNull
    @Override
    public String arg() {
        return "-lckusr";
    }

    @NotNull
    @Override
    public String command() {
        return "lock-user";
    }

    @NotNull
    @Override
    public String description() {
        return "Locked users";
    }

    @Override
    public void handler(final ConsoleEvent event) throws Exception {
        @Nullable final Session session = sessionService.getSession();
        System.out.println("[LOCK USER]");
        System.out.println("[ENTER LOGIN]");
        @Nullable final String login = TerminalUtil.nextLine();
        adminUserEndpoint.lockUser(session, login);
        System.out.println("[OK]");
    }

}
