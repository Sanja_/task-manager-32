package ru.karamyshev.taskmanager.listener.data.xml.jax;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.listener.AbstractListener;
import ru.karamyshev.taskmanager.endpoint.AdminEndpoint;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.service.SessionService;

@Component
public class DataJaxXmlClearListener extends AbstractListener {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String command() {
        return "data-jax-xml-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove json(faster) file.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJaxXmlClearListener.command() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[REMOVE XML(JAX-B) FILE]");
        @Nullable final Session session = sessionService.getSession();
        adminEndpoint.clearDataJaxBXml(session);
        System.out.println("[OK]");
    }

}
