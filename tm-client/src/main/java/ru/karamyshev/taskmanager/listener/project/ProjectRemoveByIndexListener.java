package ru.karamyshev.taskmanager.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.endpoint.Project;
import ru.karamyshev.taskmanager.endpoint.ProjectEndpoint;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.listener.AbstractListener;
import ru.karamyshev.taskmanager.service.SessionService;
import ru.karamyshev.taskmanager.util.TerminalUtil;

@Component
public class ProjectRemoveByIndexListener extends AbstractListener {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String arg() {
        return "-prtrmvind";
    }

    @NotNull
    @Override
    public String command() {
        return "project-remove-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by index.";
    }

    @Override
    @EventListener(condition = "@projectRemoveByIndexListener.command() == #event.name")
    public void handler(ConsoleEvent event) throws Exception {
        @Nullable final Session session = sessionService.getSession();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT INDEX FOR DELETION:");
        @Nullable final Integer index = TerminalUtil.nextNumber();
        @Nullable final Project project = projectEndpoint.removeOneProjectByIndex(session, index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
