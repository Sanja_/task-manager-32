package ru.karamyshev.taskmanager.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.endpoint.AuthenticationEndpoint;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.listener.AbstractListener;
import ru.karamyshev.taskmanager.service.SessionService;
import ru.karamyshev.taskmanager.util.TerminalUtil;

@Component
public class RenameLoginListener extends AbstractListener {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private AuthenticationEndpoint authenticationEndpoint;

    @NotNull
    @Override
    public String arg() {
        return "rnm-lgn";
    }

    @NotNull
    @Override
    public String command() {
        return "rename-login";
    }

    @NotNull
    @Override
    public String description() {
        return "Rename login account";
    }

    @Override
    @EventListener(condition = "@renameLoginListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        @Nullable final Session session = sessionService.getSession();
        System.out.println("CHANGE ACCOUNT LOGIN");
        System.out.println("[ENTER NEW LOGIN]");
        @Nullable final String newLogin = TerminalUtil.nextLine();
        authenticationEndpoint.renameLogin(session, newLogin);
        System.out.println("[OK]");
    }

}
