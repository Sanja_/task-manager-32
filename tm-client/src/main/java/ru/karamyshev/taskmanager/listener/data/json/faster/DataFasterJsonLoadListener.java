package ru.karamyshev.taskmanager.listener.data.json.faster;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.listener.AbstractListener;
import ru.karamyshev.taskmanager.endpoint.AdminEndpoint;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.service.SessionService;

@Component
public class DataFasterJsonLoadListener extends AbstractListener {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String command() {
        return "data-fs-json-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to base64 file.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataFasterJsonLoadListener.command() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[DATA JSON(FASTER) LOAD]");
        @Nullable final Session session = sessionService.getSession();
        adminEndpoint.loadDataFasterJson(session);
        System.out.println("[OK]");
        System.out.println("[YOU ARE LOGGED OUT]");
    }

}
