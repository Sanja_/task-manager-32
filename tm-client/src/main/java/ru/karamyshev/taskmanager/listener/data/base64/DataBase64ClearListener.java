package ru.karamyshev.taskmanager.listener.data.base64;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.listener.AbstractListener;
import ru.karamyshev.taskmanager.endpoint.AdminEndpoint;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.service.SessionService;

import java.io.Serializable;

@Component
public class DataBase64ClearListener extends AbstractListener{

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Override
    public String arg() {
        return "-dtbsclr";
    }

    @NotNull
    @Override
    public String command() {
        return "data-base64-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove base64 file.";
    }


    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBase64ClearListener.command() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[REMOVE BASE64 FILE]");
        @Nullable final Session session = sessionService.getSession();
        adminEndpoint.clearDataBase64(session);
        System.out.println("[OK]");
    }

}
