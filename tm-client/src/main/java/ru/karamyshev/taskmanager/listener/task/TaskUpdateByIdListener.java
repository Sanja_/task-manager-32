package ru.karamyshev.taskmanager.listener.task;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.Task;
import ru.karamyshev.taskmanager.endpoint.TaskEndpoint;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.listener.AbstractListener;
import ru.karamyshev.taskmanager.service.SessionService;
import ru.karamyshev.taskmanager.util.TerminalUtil;

@Component
public class TaskUpdateByIdListener extends AbstractListener {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String arg() {
        return "-tskupid";
    }

    @NotNull
    @Override
    public String command() {
        return "task-update-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by id.";
    }

    @Override
    @EventListener(condition = "@taskUpdateByIdListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        @Nullable final Session session = sessionService.getSession();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER TASK-ID:");
        @Nullable final String idTask = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final Task taskUpdate = taskEndpoint
                .updateTaskById(session, idTask, name, description);
        if (taskUpdate == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}
