package ru.karamyshev.taskmanager.listener.data.json.jax;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.listener.AbstractListener;
import ru.karamyshev.taskmanager.endpoint.AdminEndpoint;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.service.SessionService;

import java.io.Serializable;

@Component
public class DataJaxJsonClearListener extends AbstractListener implements Serializable {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String command() {
        return "data-jax-json-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove json(jax-b) file.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJaxJsonClearListener.command() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[REMOVE JSON(JAX-V) FILE]");
        @Nullable final Session session = sessionService.getSession();
        adminEndpoint.clearDataJaxBJson(session);
        System.out.println("[OK]");
    }

}
