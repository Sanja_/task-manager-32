package ru.karamyshev.taskmanager.listener.data.json.jax;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.listener.AbstractListener;
import ru.karamyshev.taskmanager.endpoint.AdminEndpoint;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.service.SessionService;

@Component
public class DataJaxJsonSaveListener extends AbstractListener {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String command() {
        return "data-jax-json-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data from json(jax-b) file.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJaxJsonSaveListener.command() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[DATA JSON(JAX-B) SAVE]");
        @Nullable final Session session = sessionService.getSession();
        adminEndpoint.saveDataJaxBJson(session);
        System.out.println("[OK]");
    }

}
