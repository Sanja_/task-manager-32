package ru.karamyshev.taskmanager.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.SessionEndpoint;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.listener.AbstractListener;
import ru.karamyshev.taskmanager.service.SessionService;

@Component
public class LogoutListener extends AbstractListener {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @NotNull
    @Override
    public String arg() {
        return "-lgout";
    }

    @NotNull
    @Override
    public String command() {
        return "logout";
    }

    @NotNull
    @Override
    public String description() {
        return "Logout in account.";
    }

    @Override
    public void handler(ConsoleEvent event) throws Exception {
        System.out.println("[LOGOUT]");
        @Nullable final Session session = sessionService.getSession();
        sessionEndpoint.closeSession(session);
        sessionService.clearSession();
        System.out.println("[OK]");
    }

}
