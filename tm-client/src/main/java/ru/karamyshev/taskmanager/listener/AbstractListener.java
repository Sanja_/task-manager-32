package ru.karamyshev.taskmanager.listener;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.event.ConsoleEvent;

@Component
public abstract class AbstractListener {

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String command();

    @Nullable
    public abstract String description();

    public abstract void handler(final ConsoleEvent event) throws Exception;
}
