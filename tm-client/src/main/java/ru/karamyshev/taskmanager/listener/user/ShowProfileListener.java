package ru.karamyshev.taskmanager.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.User;
import ru.karamyshev.taskmanager.endpoint.UserEndpoint;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.listener.AbstractListener;
import ru.karamyshev.taskmanager.service.SessionService;

@Component
public class ShowProfileListener extends AbstractListener {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @NotNull
    @Override
    public String arg() {
        return "-shw-prfl";
    }

    @NotNull
    @Override
    public String command() {
        return "show-profile";
    }

    @NotNull
    @Override
    public String description() {
        return "Show profile.";
    }

    @Override
    @EventListener(condition = "@showProfileListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        @Nullable final Session session = sessionService.getSession();
        @Nullable final User currentUser = userEndpoint.findUserById(session);
        System.out.println("[SHOW PROFILE]");
        System.out.println("LOGIN: " + currentUser.getLogin());
        System.out.println("Role: " + currentUser.getRole());
        System.out.println("HASH PASSWORD: " + currentUser.getPasswordHash());
        System.out.println("[OK]");
    }

}
