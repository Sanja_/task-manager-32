package ru.karamyshev.taskmanager.listener.data.binary;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.listener.AbstractListener;
import ru.karamyshev.taskmanager.endpoint.AdminEndpoint;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.service.SessionService;

import java.io.Serializable;

@Component
public class DataBinaryClearListener extends AbstractListener implements Serializable {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @NotNull
    @Override
    public String arg() {
        return "data-bin-clear";
    }

    @NotNull
    @Override
    public String command() {
        return "-dtbnclr";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove binary file.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBinaryClearListener.command() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[REMOVE BINARY FILE]");
        @Nullable final Session session = sessionService.getSession();
        adminEndpoint.clearDataBinary(session);
        System.out.println("[OK]");
    }

}
