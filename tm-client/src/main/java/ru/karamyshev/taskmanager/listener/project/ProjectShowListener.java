package ru.karamyshev.taskmanager.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.endpoint.*;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.listener.AbstractListener;
import ru.karamyshev.taskmanager.service.SessionService;

import java.lang.Exception;
import java.util.List;

@Component
public class ProjectShowListener extends AbstractListener {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String arg() {
        return "-prlst";
    }

    @NotNull
    @Override
    public String command() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list.";
    }

    @Override
    @EventListener(condition = "@projectShowListener.command() == #event.name")
    public void handler(ConsoleEvent event) throws Exception {
        @Nullable final Session session = sessionService.getSession();
        System.out.println("[PROJECT LIST]");
        @Nullable final List<Project> projects = projectEndpoint.findAllProject(session);
        int index = 1;
        for (Project project : projects) {
            System.out.println(index + ". " + project.getName());
            index++;
        }
        System.out.println("[OK]");
    }

}
