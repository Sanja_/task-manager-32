package ru.karamyshev.taskmanager.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.SessionEndpoint;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.exception.empty.LoginFailedException;
import ru.karamyshev.taskmanager.listener.AbstractListener;
import ru.karamyshev.taskmanager.service.SessionService;
import ru.karamyshev.taskmanager.util.TerminalUtil;

@Component
public class LoginListener extends AbstractListener {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @NotNull
    @Override
    public String arg() {
        return "-lgin";
    }

    @NotNull
    @Override
    public String command() {
        return "login";
    }

    @NotNull
    @Override
    public String description() {
        return "Login in account.";
    }

    @Override
    @EventListener(condition = "@loginListener.command() == #event.name")
    public void handler(ConsoleEvent event) throws Exception {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD");
        @Nullable final String password = TerminalUtil.nextLine();
        @Nullable final Session session = sessionEndpoint.openSession(login, password);
        if (session == null) throw new LoginFailedException();
        sessionService.setSession(session);
        System.out.println("[OK]");
    }

}
