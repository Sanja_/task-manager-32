package ru.karamyshev.taskmanager.util;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.exception.CommandIncorrectException;
import ru.karamyshev.taskmanager.exception.IndexIncorrectException;

import java.util.Scanner;

@Component
public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    @Nullable
    static String nextLine() {
        final String command = SCANNER.nextLine();
        try {
            return command;
        } catch (Exception e) {
            throw new CommandIncorrectException(command);
        }
    }

    @Nullable
    static Integer nextNumber() {
        final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new IndexIncorrectException(value);
        }
    }

}
